Prebake Custom
=======

A set of Adblock filters to block obtrusive EU cookie law notices. This is a fork from the original Prebake files because the updates there were many months behind and the list becoming bloated with outdated records, therefore I decided to make my own fork. It's as much as possible updated and cleaned up. Please go ahead and use it.

The goal of my version is to have a list that is updated frequently and is kept clean by checking the existing records from time to time and sorting the up-to-date ones as good as possible. I will add LC: yearmonthday dates to the records that have been check to give an idea when the last time was when that particular record was checked. I will personally test additions that have been committed by other people, before I actually add them.

Details
-------

I decided to maintain only one version, and nothing an obtrusive and quiet list as the original author did. I declare war to any kind of pointless messages, and even the ones that even give you a choice whether you wat to allow the site to create cookies on your device. If you don't want them, there there are already plenty of options available to block cookies (per site) in a more clear and effective way.

User Agreement
--------------

By using these filters, you are accepting and allowing sites to
set cookies by default. You agree to allow the sites you visit to
set cookies.

Only use these filters if you understand the implications of
allowing cookies.
