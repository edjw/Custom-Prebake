You can contribute directly to my fork by respecting the below rules, or directly to the original version by reading the text below.

There are going to be sites that I have missed, or I may have just screwed up with a CSS selector. In any case, feel free to point out bugs or request new sites to be added. Please make sure to test the site without any other AdBlock rules or content blockers, using the most recent version of the Custom Prebake ruleset before reporting bugs.

When adding a new filter, keep the list sorted and update the last modified timestamp to match the current date. Please also make sure you don't introduce duplicate rules: Redundancy check for Adblock Plus rules.